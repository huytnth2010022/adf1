package Bai2;

import java.util.Scanner;
import java.util.StringTokenizer;

public class Bai2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("nhập vào một chuỗi");
        String line=sc.nextLine();
        StringTokenizer stringTokenizer = new StringTokenizer(line, " !");
        while (stringTokenizer.hasMoreTokens()){
            System.out.println(stringTokenizer.nextToken());
        }

    }
}
